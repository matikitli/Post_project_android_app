package com.example.matikitli.postapp;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import 	java.net.URL;
import java.net.HttpURLConnection;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;

import org.apache.http.client.fluent.Response;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;


public class PostListActivity extends AppCompatActivity {
    EditText textUrl, textPath;
    Spinner spinner;
    Button button;
    TextView textResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);
        textUrl = (EditText) findViewById(R.id.textUrl);
        textPath = (EditText) findViewById(R.id.textPath);
        spinner = (Spinner) findViewById(R.id.spinner);
        button = (Button) findViewById(R.id.button1);
        textResult =(TextView) findViewById(R.id.textResult);
        //stworzenie listy do spinera
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.methodArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = textUrl.getText().toString();
                String path = textPath.getText().toString();
                String method = spinner.getSelectedItem().toString();
                new HttpAsyncTaskMethod().execute(url+path,method);

            }
        });

    }

    public static String METHOD(String... param){
        InputStream inputStream = null;
        String result = "";
        HttpResponse httpResponse=null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            switch (param[1]){
                case "GET": httpResponse = httpclient.execute(new HttpGet(param[0]));
                    break;
                case "PUT": httpResponse = httpclient.execute(new HttpPut(param[0]));
                    break;
                case "DELETE": httpResponse = httpclient.execute(new HttpDelete(param[0]));
                    break;
            }

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;

    }

    private class HttpAsyncTaskMethod extends AsyncTask<String,Void, String>{
        @Override
        protected String doInBackground(String... param) {
            return METHOD(param[0],param[1]);
        }
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getBaseContext(), "Otrzymano cos!", Toast.LENGTH_LONG).show();
            textResult.setText(result);
        }
    }



}




